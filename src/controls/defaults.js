/* eslint-disable no-unused-vars */
const defaults = {
  selector: '*',
  controlParams(source) {
    return [{}]
  },
  trigger(source, control) {
    // fired when tap on control
  }
}
/* eslint-enable */

export default defaults
