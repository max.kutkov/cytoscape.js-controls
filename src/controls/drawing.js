import assign from '../assign'

function setControl(source) {
  const {cy, options, defaultParams} = this

  const controlParams = options.controlParams(source)
  const controls = []

  controlParams.forEach(el => {
    const control = assign({}, el, defaultParams)

    if (!control.hasOwnProperty('position')) {
      if (source.isNode()) {
        const p = source.position()
        const w = source.width()
        const h = source.height()
        control.position = {x: p.x + w, y: p.y - h}
      } else {
        const p = source.midpoint()
        control.position = {x: p.x, y: p.y}
      }
    }

    controls.push(control)
  })

  cy.batch(() => {
    this.removeControls()
    this.controlNodes = cy.add(controls)
    this.controlNodes.addClass('control')
  })

  return this
}

function removeControls() {
  if (this.controlNodes.nonempty()) {
    this.controlNodes.remove()
  }

  return this
}

export default {setControl, removeControls}
