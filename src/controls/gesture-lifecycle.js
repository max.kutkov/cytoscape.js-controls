function canStartOn(element) {
  const {options, controlNodes, enabled} = this
  const isControl = el => controlNodes.anySame(el)
  const userFilter = el => el.filter(options.selector).nonempty()
  return (enabled && !isControl(element) && userFilter(element))
}

function show(element) {
  if (!this.canStartOn(element)) {
    return
  }
  this.sourceElement = element
  this.setControl(element)
  return this
}

function hide() {
  this.removeControls()
  return this
}

function trigger(node) {
  this.emit('trigger', this.mp(), this.sourceElement, node)
  return this
}

export default {show, canStartOn, hide, trigger}
