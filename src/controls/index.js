import defaults from './defaults'
import assign from '../assign'
import cyListeners from './cy-listeners'
import listeners from './listeners'
import enabling from './enabling'
import drawing from './drawing'
import gestureLifecycle from './gesture-lifecycle'

function Controls(cy, options) {
  this.cy = cy
  this.listeners = []

  // controls gesture state
  this.enabled = true

  // mouse position
  this.mx = 0
  this.my = 0

  this.options = assign({}, defaults, options)

  this.controlNodes = cy.collection()
  this.sourceElement = cy.collection()
  this.defaultParams = {
    group: 'nodes',
    grabbable: false,
    selectable: false
  }

  this.addListeners()
}

let proto = Controls.prototype = {}

proto.destroy = function () {
  this.removeListeners()
}

proto.mp = function () {
  return {x: this.mx, y: this.my}
}

let extend = obj => assign(proto, obj)
const fn = [cyListeners, listeners, enabling, drawing, gestureLifecycle]
fn.forEach(extend)

export default Controls
